module ES6

start syntax CompilationUnit = cu: LAYOUTLIST Statement* statements LAYOUTLIST; 
  
syntax ImportDeclaration
	= "import" ImportClause FromClause ";"
	| "import" ModuleSpecifier ";";
	
syntax ImportClause
	= 
   Id
   | String
   | NameSpaceImport
   | NamedImports
   | Id "," NameSpaceImport
   | Id "," NamedImports;

syntax NameSpaceImport
   = "*" "as" Id;

syntax NamedImports
   = "{" "}"
   | "{" ImportsList "}"
   | "{" ImportsList "," "}";

syntax ImportsList
   = ImportSpecifier
   | ImportsList "," ImportSpecifier;

syntax ImportSpecifier
   = Id
   | Id "as" Id;

syntax ModuleSpecifier
	= String;

syntax FromClause
	= "from" ModuleSpecifier;

syntax ExportDeclaration
   = "export" "*" FromClause ";"
   | "export" ExportClause FromClause ";"
   | "export" ExportClause ";"
   | "export" VarDecl
   | "export" Function
   | "export" ClassDeclaration;
   // | "export" "default" HoistableDeclaration[Default]
   // | "export" "default" ClassDeclaration[Default];
   // | "export" "default" [lookahead ∉ { function, class }] AssignmentExpression[In] ;

syntax ExportClause
   = "{" "}"
   | "{" ExportsList "}"
   | "{" ExportsList "," "}";

syntax ExportsList
   = ExportSpecifier
   | ExportsList "," ExportSpecifier;

syntax ExportSpecifier
   = Id
   | Id "as" Id;

syntax ExpressionList
   = Expression
   | ExpressionList "," Expression;

syntax CoverParenthesizedExpressionAndArrowParameterList
  = "(" ExpressionList ")"
  | "(" ")"
  | "(" "..." Id ")"
  | "(" CoverParenthesizedExpressionAndArrowParameterList ")"
  | "(" Expression "," "..." Id ")";

syntax ArrowFunction
// TODO: Ask Professor how to forbid parameters followed by line terminator
  = ArrowParameters "=\>"  Expression 
  | ArrowParameters "=\>" BlockStatement; 
  
  

syntax ArrowParameters
  = Id
  | CoverParenthesizedExpressionAndArrowParameterList;

syntax ClassDeclaration
  = "class" Id ClassTail
  | "class" ClassTail;

syntax ClassExpression
  =  "class" Id? ClassTail;
syntax ClassTail
  = ClassHeritage? "{" ClassElementList "}";
// Verificar
syntax ClassHeritage
  //= "extends" LeftHandSideExpression;
  = "extends" Expression expression;


syntax ClassElementList
  = ClassElement 
  | ClassElementList ClassElement;
// todo verificar
syntax ClassElement
  = MethodDeclaration
  | "static" MethodDeclaration
  | "get" MethodDeclaration
  | "set" MethodDeclaration
;
syntax MethodDeclaration
	= Id "(" {Expression ","}* ")" "{" Statement* "}" 
	;

syntax BlockStatement
	= "{" Statement* statements "}";

syntax Statement 
  = varDecl: VarDecl varDecl
  | classDecl: ClassDeclaration classDecl
  | empty: ";"
  | block: BlockStatement
  | expression: Expression!function expression ";"
  
  
  // ES6
  | importDecl: ImportDeclaration importDecl 
  | exportDecl: ExportDeclaration exportDecl
  // Block level things
  | function: Function function
  | ifThen: "if" "(" Expression cond ")" Statement body () !>> "else" 
  | ifThenElse: "if" "(" Expression cond ")" Statement body "else" Statement elseBody
  | doWhile: "do" Statement body "while" "(" Expression cond ")" ";"
  | whileDo: "while" "(" Expression cond ")" Statement body
  | forDo: "for" "(" {Expression ","}* inits ";" {Expression ","}* conds ";" {Expression ","}* ops ")" Statement body
  | forDoDeclarations: "for" "(" VariableIdentifier {VariableDeclarationNoIn ","}+ decls ";" {Expression ","}* conds ";" {Expression ","}* ops ")" Statement body  
  | forIn: "for" "(" Expression var "in" Expression obj ")" Statement body
  | forOf: "for" "(" VariableIdentifier Expression var "of" Expression obj ")" Statement body
  | forInDeclaration: "for" "(" "var" Id varId "in" Expression obj ")" Statement body
  | with: "with" "(" Expression scope ")" Statement body

  // Non local control flow
  | returnExp: "return"  Expression result ";"
  | returnNoExp: "return" ";"
  | throwExp: "throw" Expression result ";"
  | throwNoExp: "throw" ";"
  | continueLabel: "continue" Id label ";"
  | continueNoLabel: "continue" ";"
  | breakLabel: "break" Id label ";"
  | breakNoLabel: "break" ";"
  | debugger: "debugger" ";"
  | labeled: Id label ":" Statement statement
 
  | switchCase: "switch" "(" Expression cond ")" "{" CaseClause* clauses "}"
  | tryCatch: "try" Statement body "catch" "(" Id varId ")" Statement catchBody
  | tryFinally: "try" Statement body "finally" Statement finallyBody
  | tryCatchFinally: "try" Statement body "catch" "(" Id varId ")" Statement catchBody "finally" Statement finallyBody
  ;

syntax VariableDeclaration 
  =
  init: ( Id | Array | ObjectLiteral )  id "=" Expression init
  | nonInit: ( Id | Array | ObjectLiteral )  id
  
  //| initArray: Array "=" Expression init
  //| nonInitArray: Array id
  ;

syntax VariableDeclarationNoIn
  = init: Id id "=" Expression!inn init
  | class: Id id "=" ClassDeclaration class
  | nonInit: Id id
  ;


syntax CaseClause 
  = caseOf: "case" Expression guard ":" Statement* body
  | defaultCase: "default" ":" Statement* body
  ;
   
//syntax Function
//  = "function" Id name "(" {Id ","}* parameters ")" "{" Statement* statements "}"
//  | "function" "(" {Id ","}* parameters ")" "{" Statement* statements "}"
//  ;
  
syntax Function
  = "function" Id name "(" FunctionParameters parameters ")" "{" Statement* statements "}"
  | "function" "(" FunctionParameters parameters ")" "{" Statement* statements "}"
  ;

syntax FunctionParameters
	= {(IdInit| Array| ObjectLiteral) ","}*
	| "..." IdInit
	| {IdInit ","}* "," "..." IdInit
	;
	
syntax IdInit
	= Id 
	| Id "=" Expression;

// Todo: Check associativity https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Operator_Precedence
// Todo: Right now you can put any type of Expression on the lhs of a variableAssignment like: 5 = y; We only want to do this for a few cases however
// Rather than exclude everything other than those cases it would be much easier to whitelist the few that ARE allowed.

syntax Array
	 //="[" {Expression ","}*  elements  ","? "]";
	 = "[" ","* ElementList ","* "]"
	 | "[" Expression "]"
	 | "["  "]"
	 | "[" ElementList ","+ LastElement "]"
	 | "[" LastElement "]"
	 
	 ;
	 
syntax ElementList
	=  Expression ","+ {Expression ","}* ","*
	;

	
syntax LastElement
	= "..." Id;

syntax ExpressionListEllipse
	= {Expression ","}*
	| {Expression ","}* "," "..." Id
	| "..." Id;

syntax IdReserved
	= Reserved
	| Id
	;

syntax ObjectLiteral
	= "{" {Id ","}* properties ","? "}"
	| "{" {PropertyAssignment ","}* properties ","? "}";

syntax Expression
  = array: Array
  | objectDefinition: "{" {PropertyAssignment ","}* properties ","? "}" 
  | this: "this"
  | var: Id name
  | super: "super" >> "(" name
  | literal: Literal literal
  // ES6
  //| arrowFunction: ArrowFunction arrowFunction
  > arrowExpression: ArrowParameters "=\>"  Expression 
  > arrowParameter: ArrowParameters "=\>" BlockStatement
  // | coverParenthesizedExpressionAndArrowParameterList: CoverParenthesizedExpressionAndArrowParameterList coverParenthesizedExpressionAndArrowParameterList
  

  | bracket \bracket: "(" Expression arg ")" 
  | function: Function function
  > property: Expression obj "." IdReserved fieldId 
  | call: Expression func "(" ExpressionListEllipse params ")"
  | callInterpolation: Expression func [\u0060] BackTickStringChar* params [\u0060]
  | member: Expression obj "[" Expression field "]" 
  > new: "new" Expression cons
  > postIncr: Expression arg "++"
  | postDec: Expression arg "--"
  > delete: "delete" Expression arg
  | typeof: "typeof" Expression arg
  | preIncr: "++" Expression arg
  | preDecr: "--" Expression arg
  | prefixPlus: "+" !>> [+=] Expression arg
  | prefixMin: "-" !>> [\-=] Expression arg
  | binNeg: "~" Expression arg
  | not: "!" !>> [=] Expression arg

  
  >
  left (
      mul: Expression lhs "*" !>> [*=] Expression rhs
    | div: Expression lhs "/" !>> [/=] Expression rhs
    | rem: Expression lhs "%" !>> [%=] Expression rhs
  )
  >
  left (
      add: Expression lhs "+" !>> [+=]  Expression rhs
    | sub: Expression lhs "-" !>> [\-=] Expression rhs
  )
  > // right???
  left (
      shl: Expression lhs "\<\<" Expression rhs
    | shr: Expression lhs "\>\>" !>> [\>] Expression rhs
    | shrr: Expression lhs "\>\>\>" Expression rhs
  )
  >
  non-assoc (
      lt: Expression lhs "\<" Expression rhs
    | leq: Expression lhs "\<=" Expression rhs
    | gt: Expression lhs "\>"  Expression rhs
    | geq: Expression lhs "\>=" Expression rhs
    | instanceof: Expression lhs "instanceof" Expression rhs
    | inn: Expression lhs "in" Expression rhs
  )
  >
  right (
      eqq: Expression lhs "===" Expression rhs
    | neqq: Expression lhs "!==" Expression rhs
    | eq: Expression lhs "==" !>> [=] Expression rhs
    | neq: Expression lhs "!=" !>> [=] Expression rhs
  )
  > right binAnd: Expression lhs "&" !>> [&=] Expression rhs
  > right binXor: Expression lhs "^" !>> [=] Expression rhs
  > right binOr: Expression lhs "|" !>> [|=] Expression rhs
  > left and: Expression lhs "&&" Expression rhs
  > left or: Expression lhs "||" Expression rhs
  > cond: Expression!cond cond "?" Expression!cond then ":" Expression elseExp
  > right (
      assign: Expression lhs "=" !>> ([=][=]?) Expression rhs
    | assignMul: Expression lhs "*=" Expression rhs
    | assignDiv: Expression lhs "/=" Expression rhs
    | assignRem: Expression lhs "%=" Expression rhs
    | assignAdd: Expression lhs "+=" Expression rhs
    | assignSub: Expression lhs "-=" Expression rhs
    | assignShl: Expression lhs "\<\<=" Expression rhs
    | assignShr: Expression lhs "\>\>=" Expression rhs
    | assignShrr: Expression lhs "\>\>\>=" Expression rhs
    | assignBinAnd: Expression lhs "&=" Expression rhs
    | assignBinXor: Expression lhs "^=" Expression rhs
    | assignBinOr: Expression lhs "|=" Expression rhs
  )
  ;
  
  
syntax VarDecl
  = VariableIdentifier {VariableDeclaration ","}+ declarations ";"
  
  ;
syntax VariableIdentifier
  = "var"
  | "let"
  | "const";
  

syntax PropertyName
 = id: Id name
 | string: String key
 | numeric: Numeric numeric
 ;

syntax PropertyAssignment
  = property: PropertyName name ":" Expression value
  | get: "get" PropertyName name "(" ")" "{" Statement* body "}"
  | get: "get" "(" ExpressionListEllipse ")" "{" Statement* body "}"
  | \set: "set" PropertyName name "(" Id x ")" "{" Statement* body "}"
  ;


syntax Literal
 = null: "null"
 | boolean: Boolean bool
 | numeric: Numeric num
 | string: String str
 | regexp: RegularExpression regexp
 ;

syntax Boolean
  = t: "true"
  | f: "false"
  ;

syntax Numeric
  = decimal: [a-zA-Z$_0-9] !<< Decimal decimal
  | hexadecimal: [a-zA-Z$_0-9] !<< HexInteger hexInt
  ;

lexical TemplateCharacters
  = TemplateCharacter TemplateCharacters
  | TemplateCharacter;

lexical TemplateCharacter
  // $ [lookahead diff from {] TODO ASK PROFESSOR ABOUT LOOKAHEAD
   = [\\] EscapeSequence
  | LineContinuation
  | LineTerminatorSequence
  | SourceCharacter \ NotTemplateCharacter
  | ([\a00] | ![\n \a0D \" \\])+ \ NotTemplateCharacter;

lexical SourceCharacter
  = unicodeEscape: "\\" [u]+ [0-9 A-F a-f] [0-9 A-F a-f] [0-9 A-F a-f] [0-9 A-F a-f];

lexical NotTemplateCharacter
  = [\u0060]
  | [\\] 
  | "$" 
  | LineTerminator;

lexical LineContinuation
  = [\\] LineTerminatorSequence;

lexical LineTerminator =
  [\n] 
  | EndOfFile !>> ![] 
  | [\a0D] [\n] 
  | CarriageReturn !>> [\n] 
  ;

lexical CarriageReturn =
  [\a0D] 
  ;

lexical EndOfFile =
  
  ;

//TODO ASK PROFESSOR ABOUT DIFFERENCES
lexical LineTerminatorSequence =
  [\n] 
  | EndOfFile !>> ![] 
  | [\a0D] [\n] 
  | CarriageReturn !>> [\n] 
  ;

lexical Decimal
  = DecimalInteger [.] [0-9]* ExponentPart?
  | [.] [0-9]+ ExponentPart?
  | DecimalInteger ExponentPart?
  ;

lexical DecimalInteger
  = [0]
  | [1-9][0-9]*
  !>> [0-9]
  ;

lexical ExponentPart
  = [eE] SignedInteger
  ;

lexical SignedInteger
  = [+\-]? [0-9]+ !>> [0-9]
  ;

lexical HexInteger
  = [0] [Xx] [0-9a-fA-F]+ !>> [a-zA-Z_]
  ;

lexical String
  = [\"] DoubleStringChar* [\"]
  | [\'] SingleStringChar* [\']
  | [\u0060] BackTickStringChar* [\u0060]
  ;

lexical BackTickStringChar
  = ![\u0060]
  | [\\] EscapeSequence
  ;
lexical DoubleStringChar
  = ![\"\\\n]
  | [\\] EscapeSequence
  ;

lexical SingleStringChar
  = ![\'\\\n]
  | [\\] EscapeSequence
  ;



lexical EscapeSequence
  = CharacterEscapeSequence
  | [0] !>> [0-9]
  | HexEscapeSequence
  | UnicodeEscapeSequence
  ;

lexical CharacterEscapeSequence
  = SingleEscapeCharacter
  | NonEscapeCharacter
  ;

lexical SingleEscapeCharacter
  = [\'\"\\bfnrtv]
  ;

lexical NonEscapeCharacter
  // SourceCharacter but not one of EscapeCharacter or LineTerminator
  = ![\n\'\"\\bfnrtv0-9xu]
  ;

lexical EscapeCharacter
  = SingleEscapeCharacter
  | [0-9]
  | [xu]
  ;


  
lexical HexDigit
  = [a-fA-F0-9]
  ;

lexical HexEscapeSequence
  = [x] HexDigit HexDigit
  ;

lexical UnicodeEscapeSequence
  = "u" HexDigit HexDigit HexDigit HexDigit
  ;

lexical RegularExpression
  = [/] RegularExpressionBody [/] RegularExpressionFlags
  ;

lexical RegularExpressionBody
  = RegularExpressionFirstChar RegularExpressionChar*
  ;

lexical RegularExpressionFirstChar
  = ![*/\[\n\\]
  | RegularExpressionBackslashSequence
  | RegularExpressionClass
  ;

lexical RegularExpressionChar
  = ![/\[\n\\]
  | RegularExpressionBackslashSequence
  | RegularExpressionClass
  ;

lexical RegularExpressionBackslashSequence
  = [\\] ![\n]
  ;

lexical RegularExpressionClass
  = [\[] RegularExpressionClassChar* [\]]
  ;

lexical RegularExpressionClassChar
  = ![\n\]\\]
  | RegularExpressionBackslashSequence
  ;

lexical RegularExpressionFlags
  = [a-zA-Z]* !>> [a-zA-Z]
  ;


lexical Whitespace
  = [\t-\n\r\ ]
  ;

lexical Comment
  = @category="Comment" "/*" CommentChar* "*/"
  | @category="Comment" "//" ![\n]*  $
  ;

lexical CommentChar
  = ![*]
  | [*] !>> [/]
  ;


lexical LAYOUT
  = Whitespace
  | Comment
  ;

layout LAYOUTLIST
  = LAYOUT*
  !>> [\t\ \n]
  !>> "/*"
  !>> "//" ;


lexical Id 
  = ([a-zA-Z$_0-9] !<< [$_a-zA-Z] [a-zA-Z$_0-9]* !>> [a-zA-Z$_0-9]) \ Reserved
  ;


keyword Reserved =
    | "break" | "do" | "in" | "typeof" | "case"
    | "else" | "instanceof" | "var" |  "catch" | "export"  
    | "new" | "void" | "class" | "extends"   | "return"  
    | "while" | "const" | "finally"  | "super" | "with"  
    | "continue" |"for" | "switch" | "yield" | "debugger"  
    | "function" | "this" | "default"  | "if" |  "throw"  
    | "delete" | "import"  | "try"
    // Future
    | "enum" | "await" | "implements" |  "package" | "protected"  
    | "interface" | "private" | "public"
    // Literal
    | "null" | "true" | "false";